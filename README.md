## Solutions of "Java web training" course

- Module 1 - [ Object oriented programming ](https://bitbucket.org/cam1x/java-web-training/src/master/tasks/module1/)
- Module 2 - [ Text parsing and sorting ](https://bitbucket.org/cam1x/java-web-training/src/master/tasks/module2/)
- Module 3 - [ XML\XSD & Web-Parsing ](https://bitbucket.org/cam1x/java-web-training/src/master/tasks/module3/)
- Module 4 - [ Multithreading ](https://bitbucket.org/cam1x/java-web-training/src/master/tasks/module4/)
- Final - [ Online restaurant ](https://bitbucket.org/cam1x/java-web-training/src/master/tasks/online-restaurant/)